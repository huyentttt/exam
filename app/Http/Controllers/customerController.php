<?php

namespace App\Http\Controllers;

use App\Models\customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


class customerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');

        $listCustomer = customer::where('name', 'like', "%$search%")->paginate(3);

        return view('customer.index', [
            "listCustomer" => $listCustomer,
            "search" => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate(
            [

                'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048|dimensions:min_width=100,min_height=100,max_width=2000,max_height=2000',

                'name' => 'required|max:255',

                'phone' => 'required',
                'email' => 'required|unique:customer',

            ],
            [

                'email.unique' => 'Email đã tồn tại!',

                'image.required' => 'Ảnh đại diện không được để trống',
                'name.required' => 'Họ tên không được để trống',

                'phone.required' => 'SĐT không được để trống',
                'email.required' => 'Email không được để trống',
                'gender.required' => 'Không được để trống phần giới tính',


            ]
        );

        // Nhận dữ liệu
        $image = $request->file('image');
        $name = $request->get('name');
        $gender = $request->get('gender');
        $phone = $request->get('phone');
        $email = $request->get('email');


        $customer = new customer();

        $new_image = time() . '.' . $image->getClientOriginalExtension();
        $request->image->move(public_path('images'), $new_image);

        $customer->image = $new_image;

        $customer->name = $name;
        $customer->gender = $gender;
        $customer->phone = $phone;
        $customer->email = $email;
        $customer->save();


        return Redirect::route('customer.index');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}
