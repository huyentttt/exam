<h1>Danh sách khách hàng</h1>
<a href="{{route('customer.create')}}">Thêm khách hàng</a>

<form action="" method="get">
    <input type="text" name="search" value="#">
    <button>Tìm nó</button>
</form>

<table>
    <tr>
        <th>Mã</th>
        <th>Ảnh</th>
        <th>Tên</th>
        <th>Giới tính</th>
        <th>Số điện thoại</th>
        <th>email</th>
    </tr>
    @foreach ($listCustomer as $customer)
    <tr>
        <td>{{ $customer->id_customer }}</td>
        <td><img src="{{ asset('images/' . $customer->image) }}" width="100px"></td>
        <td>{{ $customer->name }}</td>
        <td>{{ $customer->gender }}</td>
        <td>{{ $customer->phone }}</td>
        <td>{{ $customer->email }}</td>
    </tr>
    @endforeach
</table>

{{ $listCustomer->appends([
        'search' => $search,
    ])->links() }}