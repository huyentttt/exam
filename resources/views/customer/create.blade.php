<h1>Thêm khách hàng</h1>
@if ($errors->any())

<div class="alert alert-danger">

    <ul>

        @foreach ($errors->all() as $error)

        <li>{{ $error }}</li>

        @endforeach

    </ul>

</div>

@endif
<form method="POST" action="{{route('customer.store')}}" enctype='multipart/form-data'>
    @csrf
    <table border="1">
        <tr>
            <td>Hình ảnh:</td>
            <td><input type="file" name="image"><br></td>
        </tr>
        <tr>
            <td>Tên khách hàng:</td>
            <td> <input type="text" required name="name"><br></td>
        </tr>
        <tr>
            <td>Giới tính:</td>
            <td> <input type="radio" name="gender" value="1">Nam
                <input type="radio" name="gender" value="0">Nữ<br>
            </td>
        </tr>
        <tr>
            <td>Số điện thoại:</td>
            <td> <input type="text" name="phone"><br></td>
        </tr>
        <tr>
            <td>Email:</td>
            <td> <input type="email" name="email"><br></td>
        </tr>

        <tr>
            <td><button>Submit</button></td>
        </tr>
    </table>
</form>