<h1>Thêm khách hàng</h1>
<?php if($errors->any()): ?>

<div class="alert alert-danger">

    <ul>

        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

        <li><?php echo e($error); ?></li>

        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </ul>

</div>

<?php endif; ?>
<form method="POST" action="<?php echo e(route('customer.store')); ?>" enctype='multipart/form-data'>
    <?php echo csrf_field(); ?>
    <table border="1">
        <tr>
            <td>Hình ảnh:</td>
            <td><input type="file" name="image"><br></td>
        </tr>
        <tr>
            <td>Tên khách hàng:</td>
            <td> <input type="text" required name="name"><br></td>
        </tr>
        <tr>
            <td>Giới tính:</td>
            <td> <input type="radio" name="gender" value="1">Nam
                <input type="radio" name="gender" value="0">Nữ<br>
            </td>
        </tr>
        <tr>
            <td>Số điện thoại:</td>
            <td> <input type="text" name="phone"><br></td>
        </tr>
        <tr>
            <td>Email:</td>
            <td> <input type="email" name="email"><br></td>
        </tr>

        <tr>
            <td><button>Submit</button></td>
        </tr>
    </table>
</form><?php /**PATH D:\xampp\htdocs\adminitration\resources\views/customer/create.blade.php ENDPATH**/ ?>