<h1>Danh sách khách hàng</h1>
<a href="<?php echo e(route('customer.create')); ?>">Thêm khách hàng</a>

<form action="" method="get">
    <input type="text" name="search" value="#">
    <button>Tìm nó</button>
</form>

<table>
    <tr>
        <th>Mã</th>
        <th>Ảnh</th>
        <th>Tên</th>
        <th>Giới tính</th>
        <th>Số điện thoại</th>
        <th>email</th>
    </tr>
    <?php $__currentLoopData = $listCustomer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>
        <td><?php echo e($customer->id_customer); ?></td>
        <td><img src="<?php echo e(asset('images/' . $customer->image)); ?>" width="100px"></td>
        <td><?php echo e($customer->name); ?></td>
        <td><?php echo e($customer->gender); ?></td>
        <td><?php echo e($customer->phone); ?></td>
        <td><?php echo e($customer->email); ?></td>
    </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>

<?php echo e($listCustomer->appends([
        'search' => $search,
    ])->links()); ?><?php /**PATH D:\xampp\htdocs\adminitration\resources\views/customer/index.blade.php ENDPATH**/ ?>