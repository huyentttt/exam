<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\customerController;

Route::get('/', function () {
    return view('welcome');
});

// Quản lý lớp => CRUD 
Route::resource('/customer', customerController::class);

// Quản lý sinh viên => CRUD 
